[![Unity Engine](https://img.shields.io/badge/unity-2022.2.8f1-black.svg?style=flat&logo=unity&cacheSeconds=2592000)](https://unity3d.com/get-unity/download/archive)

# Retarget Humanoid Animation

Follow the tutorial [Unity: Retarget Humanoid Animation in One Minute!](https://www.youtube.com/watch?v=9ndQdcgJ35s&ab_channel=DustinMorman).

This helps me learn about retarget humaniod animation, and it's
very useful for indie developer who don't know how to animate
in Maya or Blender.
